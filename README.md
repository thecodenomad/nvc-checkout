# Checkout for NoviCap Store



## Implementation

- I have implemented the project in Java (Spring Boot framework). The core idea behind this was to save up on time, as I am currently coding in Java, making it a lot quicker for me to complete the project.

- One key different from a ruby implementation is that I have separated classes using the following structure

  - `config` - accesing the json file
  - `models` - Plain Old Java Objects for structure of the object
  - `rules` - Rules implementation which will be injected into the `CheckoutService`
  - `services` - Service objects to implement methods on the model objects

- Note: I have not implemented logging in order to keep it simple. Instead I have implemented try/catch blocks with the exception stacktrace thrown.

- The process implemented is as follows:

  - The `ProductConfig` class reads the `products.json` file stored in the resources directory  using the `retrieveProductsFromJsonFile` method and maps all the products to the `Product` class.
  - The `CheckoutService` class creates a new `Cart` object when instantiated.
  - The `ProductService` class retrieves any individual product using the `findByCode` method when a code is passed to it while communicating with the `ProductConfig` class. This product is then added to the previous instantated `cart` using the `addToCart` method in the `CartService` class .
  - A product can be scanned scanned using the `scan()` method in  `CheckoutService` where the `code` of the product is scanned and added to the prevously instantiated cart.
  - The `CartService` class then implements the `getTotal()` method using to retrieve the total amount of all items added to the cart
  - The `CheckoutService` then implements the `getCheckoutTotal()` class to retrieve the final `Checkout` object, which includes the list of all scanned products, as well as the total amount. 
  - The `TshirtRule` class and the `VoucherRule` class are implemented in the `getCheckoutTotal()`to determine any deviations from existing pricing rules. Both these classes are children of the `Rule` class.
  - This method is implemented in the `run()` method of the `CheckoutApplication` class which displays the output on the terminal. 
  - Input:

```java
  List<Rule> rules = new ArrayList<>();

  @Autowired
  private VoucherRule voucherRule;

  @Autowired
  private TshirtRule tshirtRule;

  public List<Rule> getRules() {
    rules.add(voucherRule);
    rules.add(tshirtRule);
    return rules;
  }

  @Autowired
  private CheckoutService checkoutService = new CheckoutService(getRules());


  @Override
  public void run(String... args) throws Exception {
    try {
      checkoutService.scan("VOUCHER");
      checkoutService.scan("VOUCHER");
      checkoutService.scan("TSHIRT");
    } catch(Exception ex) {
      ex.printStackTrace();
    }

    System.out.println(checkoutService.getCheckoutTotal());
  }
```

  - Output:

```bash
  Checkout(products=[VOUCHER, VOUCHER, TSHIRT, VOUCHER], total=30.00€)
```

  

## Test cases

`getCorrectTotalPriceWithNoPricingRules()`

- Retrieves all scanned items at checkout with the total price

`getCorrectTotalPriceWithTshirtPricingRules()`

- Retrieves all scanned items at checkout with tshirt rules applied

`getCorrectTotalPriceWithVoucherPricingRules()`

- Retrieves alll scanned items at checkout with voucher rules applied

`getCorrectTotalPriceWithTshirtAndVoucherPricingRules()`

- Retrieves all scanned items at checkout with both tshirt and voucher rules applied


