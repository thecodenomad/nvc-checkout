package com.nvc.checkout;

import com.nvc.checkout.rules.Rule;
import com.nvc.checkout.rules.TshirtRule;
import com.nvc.checkout.rules.VoucherRule;
import com.nvc.checkout.services.CheckoutService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class CheckoutApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(CheckoutApplication.class, args);
    }

    private final List<Rule> rules = new ArrayList<>();

    @Autowired
    private VoucherRule voucherRule;

    @Autowired
    private TshirtRule tshirtRule;

    private List<Rule> getRules() {
        rules.add(voucherRule);
        rules.add(tshirtRule);
        return rules;
    }

    @Autowired
    private CheckoutService checkoutService = new CheckoutService(getRules());


    @Override
    public void run(String... args) {
        try {
            checkoutService.scan("VOUCHER");
            checkoutService.scan("VOUCHER");
            checkoutService.scan("TSHIRT");
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        System.out.println(checkoutService.getCheckoutTotal());
    }
}
