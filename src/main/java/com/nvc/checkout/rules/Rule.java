package com.nvc.checkout.rules;

import com.nvc.checkout.models.Cart;
import com.nvc.checkout.services.CartService;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@NoArgsConstructor
@Component
public class Rule {
    @Autowired
    private CartService cartService;

    public void getTotal(Cart cart) {
        cartService.getTotal(cart);
    }
}
