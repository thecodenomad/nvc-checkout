package com.nvc.checkout.rules;

import com.nvc.checkout.models.Cart;
import com.nvc.checkout.models.Product;
import com.nvc.checkout.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TshirtRule extends Rule {
    private static final String TSHIRT_CODE = "TSHIRT";
    private static final Double REVISED_PRICE = 19.0;
    private static final Integer TSHIRT_COUNT = 3;

    @Autowired
    private CartService cartService;

    public void getTotal(Cart cart) {
        List<Product> tshirts = cart
                .getProducts()
                .stream()
                .filter(product -> product.getCode().equals(TSHIRT_CODE))
                .collect(Collectors.toList());

        if (tshirts.size() >= TSHIRT_COUNT) {
            tshirts.forEach(tshirt -> tshirt.setPrice(REVISED_PRICE));
        }

        cartService.getTotal(cart);
    }
}
