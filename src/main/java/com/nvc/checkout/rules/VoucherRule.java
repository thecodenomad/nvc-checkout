package com.nvc.checkout.rules;

import com.nvc.checkout.models.Cart;
import com.nvc.checkout.models.Product;
import com.nvc.checkout.services.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Component
public class VoucherRule extends Rule {
    private static final String VOUCHER_CODE = "VOUCHER";
    private static final String VOUCHER_NAME = "NoviCap Voucher";

    @Autowired
    private CartService cartService;

    public void getTotal(Cart cart) {
        try {

            List<Product> vouchers = cart
                    .getProducts()
                    .stream()
                    .filter(product -> product.getCode().equals(VOUCHER_CODE))
                    .collect(Collectors.toList());

            List<Integer> intRange = IntStream.range(0, vouchers.size() / 2).boxed().collect(Collectors.toList());

            for (Integer i : intRange) {
                cartService.addToCart(cart, createFreeVoucher());
            }
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        cartService.getTotal(cart);
    }

    private Product createFreeVoucher() {
        Product freeVoucher = new Product();
        freeVoucher.setName(VOUCHER_NAME);
        freeVoucher.setCode(VOUCHER_CODE);
        freeVoucher.setPrice(0.0);
        return freeVoucher;
    }
}
