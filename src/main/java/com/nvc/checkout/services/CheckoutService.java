package com.nvc.checkout.services;

import com.nvc.checkout.rules.Rule;
import com.nvc.checkout.models.Cart;
import com.nvc.checkout.models.Checkout;
import com.nvc.checkout.models.Product;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Data
public class CheckoutService {
    List<Product> products = new ArrayList<>();
    private List<Rule> rules;
    private Cart cart;

    @Autowired
    private ProductService productService;

    @Autowired
    private CartService cartService;

    public CheckoutService(List<Rule> rules) {
        cart = new Cart();
        setRules(rules);
    }

    public void scan(String code) {
        Product product = null;

        try {
            product = productService.findByCode(code);
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        cartService.addToCart(cart, product);
    }

    public Checkout getCheckoutTotal() {
        rules.forEach(rule -> rule.getTotal(cart));
        Checkout checkout = new Checkout();

        try {
            cart.getProducts().forEach(product -> checkout.getProducts().add(product.getCode()));
            checkout.setTotal(String.format("%.2f", cartService.getTotal(cart)) + "€");
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return checkout;
    }

}
