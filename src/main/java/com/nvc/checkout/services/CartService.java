package com.nvc.checkout.services;

import com.nvc.checkout.models.Cart;
import com.nvc.checkout.models.Product;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CartService {
    public void addToCart(Cart cart, Product product) {
        if (null != cart) {
           cart.getProducts().add(product);
        }
    }

    public Double getTotal(Cart cart) {
        Double total = null;

        try {
            List<Double> prices = new ArrayList<>();
            new ArrayList<>(cart.getProducts())
                    .forEach(product -> prices.add(product.getPrice()));
            total = prices.stream().reduce(0d, Double::sum);
        } catch(Exception ex) {
            ex.printStackTrace();
        }

        return total;
    }

}
