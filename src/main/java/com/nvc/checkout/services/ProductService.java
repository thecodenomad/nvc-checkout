package com.nvc.checkout.services;

import com.nvc.checkout.config.ProductConfig;
import com.nvc.checkout.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    private ProductConfig productConfig;

    public Product findByCode(String code) {
        return productConfig
            .retrieveProductsFromJsonFile()
            .stream()
            .filter(product -> product.getCode().equals(code))
            .collect(Collectors.toList()).get(0);
    }
}
