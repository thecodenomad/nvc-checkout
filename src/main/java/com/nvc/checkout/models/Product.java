package com.nvc.checkout.models;

import lombok.Data;

@Data
public class Product {
    private String code;
    private String name;
    private Double price;
}
