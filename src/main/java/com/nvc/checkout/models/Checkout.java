package com.nvc.checkout.models;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Checkout {
    private List<String> products = new ArrayList<>();
    private String total;
}
