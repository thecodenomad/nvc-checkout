package com.nvc.checkout.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nvc.checkout.models.Product;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProductConfig {
    private final Path productConfigFilePath = Paths.get("./src/main/resources");

    public List<Product> retrieveProductsFromJsonFile() {
        ObjectMapper objectMapper = new ObjectMapper();
        String productConfigFileName = "/products.json";
        File file = new File(productConfigFilePath + productConfigFileName);
        List<Product> products= new ArrayList<>();

        try {
            return objectMapper.readValue(file, objectMapper
                .getTypeFactory()
                .constructCollectionType(List.class, Product.class));
        } catch(IOException ex) {
            ex.printStackTrace();
        }

        return products;
    }

}
