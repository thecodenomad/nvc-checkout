package com.nvc.checkout;

import com.nvc.checkout.models.Cart;

import com.nvc.checkout.services.CheckoutService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.*;

@SpringBootTest
class CheckoutApplicationTests {

    @Autowired
    private CheckoutService checkoutService;

    @Test
    public void getCorrectTotalPriceWithNoPricingRules() {
        Cart cart = new Cart();
        checkoutService.setCart(cart);
        checkoutService.scan("TSHIRT");
        checkoutService.scan("VOUCHER");
        checkoutService.scan("MUG");
        String[] scannedItems = new String[]{"TSHIRT", "VOUCHER", "MUG"};
        assertThat(checkoutService.getCheckoutTotal().getProducts()).containsExactly(scannedItems);
        assertThat(checkoutService.getCheckoutTotal().getTotal()).isEqualTo("32.50€");
    }

    @Test
    public void getCorrectTotalPriceWithTshirtPricingRules() {
        Cart cart = new Cart();
        checkoutService.setCart(cart);
        checkoutService.scan("TSHIRT");
        checkoutService.scan("TSHIRT");
        checkoutService.scan("TSHIRT");
        checkoutService.scan("VOUCHER");
        checkoutService.scan("MUG");

        String[] scannedItems = new String[]{"TSHIRT", "TSHIRT", "TSHIRT", "VOUCHER", "MUG"};
        assertThat(checkoutService.getCheckoutTotal().getProducts()).containsExactly(scannedItems);
        assertThat(checkoutService.getCheckoutTotal().getTotal()).isEqualTo("69.50€");
    }

    @Test
    public void getCorrectTotalPriceWithVoucherPricingRules() {
        Cart cart = new Cart();
        checkoutService.setCart(cart);
        checkoutService.scan("TSHIRT");
        checkoutService.scan("TSHIRT");
        checkoutService.scan("VOUCHER");
        checkoutService.scan("VOUCHER");
        checkoutService.scan("MUG");

        String[] scannedItems = new String[]{"TSHIRT", "TSHIRT", "VOUCHER", "VOUCHER", "MUG", "VOUCHER"};
        assertThat(checkoutService.getCheckoutTotal().getProducts()).containsExactly(scannedItems);
        assertThat(checkoutService.getCheckoutTotal().getTotal()).isEqualTo("57.50€");
    }

    @Test
    public void getCorrectTotalPriceWithTshirtAndVoucherPricingRules() {
        Cart cart = new Cart();
        checkoutService.setCart(cart);
        checkoutService.scan("TSHIRT");
        checkoutService.scan("TSHIRT");
        checkoutService.scan("TSHIRT");
        checkoutService.scan("VOUCHER");
        checkoutService.scan("VOUCHER");
        checkoutService.scan("MUG");

        String[] scannedItems = new String[]{"TSHIRT", "TSHIRT", "TSHIRT", "VOUCHER", "VOUCHER", "MUG", "VOUCHER"};
        assertThat(checkoutService.getCheckoutTotal().getProducts()).containsExactly(scannedItems);
        assertThat(checkoutService.getCheckoutTotal().getTotal()).isEqualTo("74.50€");
    }


}
